# electricrailmap.com

Source for static  mkdocs website for fizzy-knitting 

## Local Dev

Assume python3 in installed...

```bash
git clone git@gitlab.com:electricrailmap/electricrailmap.gitlab.io.git
cd electricrailmap.gitlab.io
pip3 install -r requirements.txt
mkdocs serve

```

then open browser at [http://127.0.0.1:8000](http://127.0.0.1:8000)