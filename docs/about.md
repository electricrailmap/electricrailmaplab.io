---
title: About
---


## Overview

- This project is built on top of and is part of [Open Street Map (OSM)](https://www.openstreetmap.org) 
- The core requirement is to show an up to date map of electrification in the UK (and decarbonisation)
- [Open Railway Map](https://www.openrailwaymap.org/) another OSM project is a more generalised view


## Brief History

Once upon a time on a hot day in May 2020, amidst a first in a generation pandemic.lockdown, 
a rail engineer [tweeted](https://twitter.com/25kV/status/1264854274506395648?ref_src=twsrc%5Etfw)......

```If anyone out there has experience of building an instance of @openstreetmap, could you get in touch? I've had a dangerous idea, but 
I don't have all the necessary technical skills to implement
```

whereafter the open source project [#fizzy-knitting](https://fizzy-knitting.gitlab.io/about/) was born, and has had lots of input,
and this is the result a few months later ...  a pandemic baby  ;-)


## Team and Thanks

thanks to the following, in no particular order as every little bit counts, including blatant plugs ;-)

- Garry Keenor - [@25kV](https://twitter.com/25kv) - the BDFL team leader ([Read the book](https://www.ocs4rail.com/) - free download)
- Pete Hicks + Cat - [@poggs](https://twitter.com/poggs) - mapping - ([OpenTrainTimes](https://www.opentraintimes.com/))
- Martin Hoffmanm ([@partim](https://github.com/partim))
- Will Deakin [@anisotropi4](https://github.com/anisotropi4) - early vector [visuals](https://bl.ocks.org/anisotropi4)
- Team of dedicated OSM taggers.. huge thanks!!
- Pedro Morgan - janitor




